mod execution;
pub mod macros;

pub use pest::Parser;
use std::collections::HashMap;

use pest::iterators::Pairs;
use pest_derive::Parser;
use std::error::Error;

pub type Result<T> = std::result::Result<T, Box<dyn Error>>;

#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct IronParser;

#[derive(Debug)]
pub enum Value {
    StringVal(String),
    NumVal(f64),
    Nothing,
}

pub fn execute(ast: Result<Pairs<Rule>>, mut variables: &mut HashMap<String, Value>) {
    match ast {
        Ok(pairs) => {
            if pairs.len() != 1 {
                dbg!("how is there more than one program in a file");
                return;
            }
            for pair in pairs {
                let inner = pair.into_inner();
                for statement in inner {
                    match statement.as_rule() {
                        Rule::statement => {
                            execution::exec_statements(statement.into_inner(), &mut variables);
                        }
                        Rule::EOI => {
                            return;
                        }
                        _ => unreachable!(),
                    }
                }
            }
        }
        Err(error) => {
            dbg!(error);
        }
    }
}
