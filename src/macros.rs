#[macro_export]
macro_rules! box_err {
    ($err:expr) => {
        match $err {
            Err(err) => Err(Box::new(err)),
            Ok(ok) => Ok(ok),
        }
    };
}

pub use box_err;
