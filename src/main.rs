use colored::Colorize;
use std::{collections::HashMap, io::{stdin, stdout, Write}};

use iron::*;

fn main() -> Result<()> {
    let startup_str = format!(
        "{} by {}. Call {} to exit.",
        "GOOFY-AH-LANGUAGE".bold().blue(),
        "Tharun".underline(),
        "exit();".italic()
    );
    println!("{startup_str}");
    let mut variables = HashMap::<String, Value>::new();
    
    loop {
        let mut input = String::new();
        print!("{}", ">>> ".bold());
        stdout().lock().flush()?;
        stdin().read_line(&mut input)?;

        if &input == "exit();\n" {
            println!("{}", "Exiting".bold().red());
            return Ok(());
        }

        execute(box_err!(IronParser::parse(Rule::program, &input)), &mut variables);
        dbg!(&variables);
    }
}
