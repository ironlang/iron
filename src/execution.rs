use std::collections::HashMap;

use pest::iterators::{Pair, Pairs};

use crate::{Rule, Value};

pub fn exec_statements(statements: Pairs<Rule>, mut variables: &mut HashMap<String, Value>) {
    for statement in statements {
        exec_statement(statement, &mut variables)
    }
}

fn exec_statement(statement: Pair<Rule>, mut variables: &mut HashMap<String, Value>) {
    match statement.as_rule() {
        Rule::var_def => exec_var_def(statement, &mut variables),
        _ => unimplemented!("i can't be bothered"),
    }
}

fn exec_var_def(var_def: Pair<Rule>, variables: &mut HashMap<String, Value>) {
    variables.insert(
        var_def
            .clone()
            .into_inner()
            .nth(0)
            .unwrap()
            .as_str()
            .to_string(),
        Value::StringVal(
            var_def
                .clone()
                .into_inner()
                .nth(1)
                .unwrap()
                .as_str()
                .to_string(),
        ),
    );
}
